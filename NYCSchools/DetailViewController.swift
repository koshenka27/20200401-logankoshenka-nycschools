//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by Logan  Koshenka on 4/1/20.
//  Copyright © 2020 Logan Koshenka. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var scoreLabel: UILabel!
    
    var school: School!
    var thisSAT: SAT? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadDetails()
    }
    
    func loadDetails() {
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error?.localizedDescription)
            } else {
                if let content = data {
                    do {
                        let myJSON = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        if let scores = myJSON as? [Dictionary<String, Any>] {
                            print(scores)
                            let filtered = scores.filter({ $0["dbn"] as! String == self.school.dbn })
                            print(filtered)
                            
                            if let thisSchool = filtered.first {
                                // only 1 school should return from filtered but it's still an array, so get the first / only one
                                
                                let schoolName = thisSchool["school_name"] as! String
                                let readingScore = thisSchool["sat_critical_reading_avg_score"] as! String
                                let writingScore = thisSchool["sat_writing_avg_score"] as! String
                                let mathScore = thisSchool["sat_math_avg_score"] as! String
                                let dbn = thisSchool["dbn"] as! String
                                self.thisSAT = SAT(schoolName: schoolName, reading: readingScore, writing: writingScore, math: mathScore, dbn: dbn)
                            }
                        }
                        DispatchQueue.main.async {
                            // update ui
                            self.titleLabel.text = self.thisSAT?.schoolName.capitalized
                            self.overviewLabel.text = self.school.overview
                            self.locationLabel.text = self.school.location
                            self.scoreLabel.text = self.thisSAT?.math
                        }
                    } catch {
                        
                    }
                }
            }
        }.resume()
    }
    
    @IBAction func didChangeSegControl(_ sender: Any) {
        // update score label with each segmented control change
        // had to make it blue because of chase ;)
        
        if segmentedControl.selectedSegmentIndex == 0 {
            self.scoreLabel.text = self.thisSAT?.math
        } else if segmentedControl.selectedSegmentIndex == 1 {
            self.scoreLabel.text = self.thisSAT?.reading
        } else if segmentedControl.selectedSegmentIndex == 2 {
            self.scoreLabel.text = self.thisSAT?.writing
        }
    }
    
}
