//
//  School.swift
//  NYCSchools
//
//  Created by Logan  Koshenka on 4/1/20.
//  Copyright © 2020 Logan Koshenka. All rights reserved.
//

import Foundation


struct School: Decodable {
    let name: String
    let overview: String
    let location: String
    let dbn: String
}
