//
//  SchoolsTableViewController.swift
//  NYCSchools
//
//  Created by Logan  Koshenka on 4/1/20.
//  Copyright © 2020 Logan Koshenka. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {

    var schoolsToDisplay = [School]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchSchools()
    }
    
    func fetchSchools() {
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error?.localizedDescription)
            } else {
                if let content = data {
                    do {
                        let myJSON = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        if let schools = myJSON as? [Dictionary<String, Any>] {
                            for school in schools {
                                if let name = school["school_name"],
                                let overview = school["overview_paragraph"],
                                let location = school["location"],
                                let dbn = school["dbn"] {
        
                                    // creating school object from school struct
                                    let schoolToAdd = School(name: name as! String, overview: overview as! String, location: location as! String,dbn: dbn as! String)
                                    
                                    // add to array to display in list
                                    self.schoolsToDisplay.append(schoolToAdd)
                                }
                            }
                        }
                        
                        // sort alphabetically
                        let sorted = self.schoolsToDisplay.sorted(by: {$0.name < $1.name })
                        self.schoolsToDisplay = sorted
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    } catch {
                        
                    }
                }
            }
        }.resume()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsToDisplay.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        var school = schoolsToDisplay[indexPath.row]
        cell.textLabel?.text = school.name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        
        // get this specific school for next page
        let school = schoolsToDisplay[indexPath.row]
        detailVC.title = school.name
        detailVC.school = school
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

}
