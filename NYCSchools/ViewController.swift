//
//  ViewController.swift
//  NYCSchools
//
//  Created by Logan  Koshenka on 4/1/20.
//  Copyright © 2020 Logan Koshenka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var highSchoolsButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        highSchoolsButton.layer.cornerRadius = highSchoolsButton.bounds.height / 2
        highSchoolsButton.clipsToBounds = true
    }


}

