//
//  SAT.swift
//  NYCSchools
//
//  Created by Logan  Koshenka on 4/1/20.
//  Copyright © 2020 Logan Koshenka. All rights reserved.
//

import Foundation


struct SAT: Decodable {
    let schoolName: String
    let reading: String
    let writing: String
    let math: String
    let dbn: String
}
